package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Employee;
import com.example.demo.exception.EmployeeNotFoundException;
import com.example.demo.service.IEmployeeService;


@Controller
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private IEmployeeService service;

	/***
	 * 1. SHOW REGISTER PAGE
	 * This method is used to display Register Page
	 * when end-user enters /register with GET Type
	 */
	@GetMapping("/register")
	public String showRegPage() {
		return "EmployeeRegister";
	}
	
	
	/**
	 * 2. ON CLICK FORM SUBMIT, READ DATA (@MODELATTRIBUTE)
	 * This method is used to read Form data as Model Attribute
	 * It will make call to service method by passing same form object
	 * Service method returns PK(ID). 
	 * Controller returns String message back to UI using Model
	 * @param employee
	 * @param model
	 * @return
	 */
	@PostMapping("/save")
	public String saveFormData(
			@ModelAttribute Employee employee,
			Model model
			) 
	{
		Integer id = service.saveEmployee(employee);
		String message = new StringBuffer().append("EMPLOYEE '")
				.append(id).append("' CREATED").toString();
				//"EMPLOYEE '"+id+"' CREATED";
		
		model.addAttribute("message", message);
		return "EmployeeRegister";
	}
	
	/***
	 * 3. Display all rows as a table
	 * This method is executed for request URL /all + GET.
	 * It will fetch data from Service as List<T>
	 * Send this data to UI(View) using Model(I)
	 * In UI use th:each="tempVariable:${collectionName}" to read data 
	 * and print as HTML Table.
	 */
//	@GetMapping("/all")
//	public String showData(Model model) {
//		List<Employee> list = service.getAllEmployees();
//		model.addAttribute("list", list);
//		return "EmployeeData";
//	}
	
	//4. Delete based on id
	
	//5. On Click Edit Link(HyperLink) Show data in Edit Form
	
	//6. Update Form data and submit
	

    @GetMapping("/all")
	public String showData(
			Model model,
			@RequestParam(value = "message", required = false) String message
			) 
	{
		List<Employee> list = service.getAllEmployees();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "EmployeeData";
	}

	@GetMapping("/delete")
	public String deleteData(
			@RequestParam("id")Integer empId,
			RedirectAttributes attributes 
			) 
	{
		String msg = null;
		try {
			service.deleteEmployee(empId);
			msg = "Employee '"+empId+"' Deleted";
		} catch (EmployeeNotFoundException e) {
			e.printStackTrace();
			msg = e.getMessage();
		}
		attributes.addAttribute("message", msg);
		return "redirect:all";
	}
}
