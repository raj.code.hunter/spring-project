package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Employee;
import com.example.demo.exception.EmployeeNotFoundException;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.IEmployeeService;


@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private EmployeeRepository repo;
	
	public Integer saveEmployee(Employee e) {
		e = repo.save(e);
		double hra = e.getEmpSal() * (12.0/100);
		double ta = e.getEmpSal() * (4.0/100);
		e.setEmpHra(hra);
		e.setEmpTa(ta);
		return e.getEmpId();
	}

	public void updateEmployee(Employee e) {
		repo.save(e);
	}

//	public void deleteEmployee(Integer id) {
//		repo.deleteById(id);
//	}

	public Employee getOneEmployee(Integer id) {
		Optional<Employee> opt = repo.findById(id);
		return opt.get();
	}

	public List<Employee> getAllEmployees() {
		List<Employee> list = repo.findAll();
		return list;
	}
	
    public void deleteEmployee(Integer id) {
		//repo.deleteById(id);
		/*Optional<Employee> opt = repo.findById(id);
		if(opt.isPresent()) {
			repo.delete(opt.get());
		} else {
			throw new EmployeeNotFoundException("EMPLOYEE '"+id+"' NOT FOUND");
		}*/
		repo.delete(
				repo.findById(id)
				.orElseThrow(
						()->new EmployeeNotFoundException("EMPLOYEE '"+id+"' NOT FOUND"))
				);
	}

}